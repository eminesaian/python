import subprocess

with open("hosts_and_ports.txt") as hp_fh:
    hp_contents = hp_fh.readlines()
    for hp_pair in hp_contents:
        x=hp_pair.split(":")
        y="ssh ubuntussh@"+ x[0]+" -p "+x[1].rstrip("\n")
        print(y)
        with open("commands.txt") as fh:
            completed = subprocess.run(y, capture_output=True, text=True, shell=True, stdin=fh)