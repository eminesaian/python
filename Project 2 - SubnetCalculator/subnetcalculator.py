import sys

Fout_message = "IP-adres en/of subnetmasker is ongeldig."
 
def ask_for_number_sequence(message):
    antwoord = input(message)
    return [int(element) for element in antwoord.split(".")]

def is_valid_ip_address(numberlist):
    block =[(print(Fout_message), sys.exit(0)) for element in numberlist if element > 255 or element < 0 or len(numberlist) != 4 ]
    return block

def is_valid_netmask(numberlist):
    
    if len(numberlist) !=4 :
        print(Fout_message)
        sys.exit(0)
 
    else:
        binary_netmask = ""
            
        for element in numberlist:
            if element > 255 or element < 0:
                print(Fout_message)
                sys.exit(0)
            block_binaire = f"{int(element):08b}"
            binary_netmask = binary_netmask + block_binaire

        checking_ones = True

        for symbol in binary_netmask :
            if  checking_ones == True and symbol == "0":
                checking_ones = False
            elif symbol == "1" and checking_ones == False:
                print(Fout_message)
                sys.exit(0)        
    return binary_netmask

def one_bits_in_netmask(binary_netmask):
    binary_netmask_l=""
    counter = 0
    
    for element in binary_netmask:
        block_binaire = str(f"{int(element):08b}")
        binary_netmask_l = binary_netmask_l + block_binaire
    for element in binary_netmask_l:
        if element == "1":           
            counter +=1 

    return counter
    
def apply_network_mask(host_address,netmask):
    binary_host = ""
    binary_netmask = ""
    i = 0
    sub_str = ""

    for block in host_address:
        block_binaire = f"{int(block):08b}"
        binary_host = binary_host + block_binaire

    
    for block in netmask:
        block_binaire = f"{int(block):08b}"
        binary_netmask = binary_netmask + block_binaire


    while i < 32:
        if binary_host[i] == "1" and binary_netmask[i] == "1":
            sub_str = sub_str + "1"
        else:
            sub_str = sub_str + "0"
        i +=1

    networkID = []
    networkID_str= int(sub_str[0:8],2), int(sub_str[8:16],2) , int(sub_str[16:24],2) , int(sub_str[24:32],2)
    networkID = [int (element) for element in networkID_str]    
    return networkID   

def netmask_to_wilcard_mask(subnet_masker):     
    wildcard_mask = []
    for element in subnet_masker:
        wildcard_bit = ""
        for bit in f"{element:08b}":
            if bit == "0":
                wildcard_bit += "1"
            else:
                wildcard_bit += "0"
        wildcard_mask.append(int(wildcard_bit,2))        
    return wildcard_mask

def get_broadcast_address(network_address,wildcard_mask):
    
    binary_network = ""
    binary_wildcard = ""
    i = 0
    sub_str = ""
    
    for block in network_address:
        block_binaire = f"{int(block):08b}"
        binary_network = binary_network + block_binaire
    
    for block in wildcard_mask:
        block_binaire = f"{int(block):08b}"
        binary_wildcard = binary_wildcard + block_binaire
     

    while i < 32:
        if binary_network[i] == "1" or binary_wildcard[i] == "1":
            sub_str = sub_str + "1"
        else:
            sub_str = sub_str + "0"
        i +=1

    broadcast = [] 
    broadcast_str= int(sub_str[0:8],2), int(sub_str[8:16],2) , int(sub_str[16:24],2) , int(sub_str[24:32],2)
    broadcast = [int (element) for element in broadcast_str]
    return broadcast

def prefix_length_to_max_hosts(aantal_1_bits):
    aantal_host = 2** (32 - aantal_1_bits) - 2
    return aantal_host
    
if __name__ == '__main__':

    host_address = ask_for_number_sequence("Wat is het IP-adres?\n")
    subnet_mask = ask_for_number_sequence("Wat is het subnetmasker?\n")
    
    is_valid_ip_address(host_address)
    is_valid_netmask(subnet_mask)
    
    if len(subnet_mask) ==4:
        print("IP-adres en subnetmasker zijn geldig.")
        
    aantal_1_bits = one_bits_in_netmask(subnet_mask)
    print("De lengte van het subnetmasker is " + str(aantal_1_bits) + ".")
    
    netwerk_address = apply_network_mask(host_address,subnet_mask)
    print("Het adres van het subnet is " + ('.'.join([str(i) for i in (netwerk_address)])) +".")
    
    wildcard_mask = netmask_to_wilcard_mask(subnet_mask)
    print("Het wildcardmasker is " + ('.'.join([str(i) for i in (wildcard_mask)]))+".")
    
    broadcast_address= get_broadcast_address(netwerk_address,wildcard_mask)
    print("Het broadcastadres is " + ('.'.join([str(i) for i in (broadcast_address)]))+".")
    
    max_anntalhosts = prefix_length_to_max_hosts(aantal_1_bits)
    print("Het maximaal aantal hosts op dit subnet is " + (str(max_anntalhosts))+".")
    
